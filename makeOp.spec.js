const assert = require('assert')
const getMakeOp = require('./makeOp')

describe('makeOp', function () {

    it('returns OPERATION_NOT_PERFORMED if update returns false', async function () {
        const makeOp = getMakeOp({
            ctx: {
                ctx: {
                    user: {}
                }
            },
            initOp: function (ctx) {
                return {
                    _id: Date.now(),
                    date: new Date(),
                    user: ctx.ctx.user,
                }
            },
            initCriteria: function (doc) {
                return {_id: doc._id}
            },
            initUpdate: function (op) {
                return {
                    '$push': {
                        'ops': op
                    }
                }
            },
            update: function (criteria, update) {
                return false
            },
            find: function (doc) {
                return doc
            },
            updateOp: function (doc, op, update) {
                return doc
            },
            resolveDone: function () {},
            resolveExtra: function () {}
        })

        try {
            await makeOp({
                id: 1
            }, {
                name: 'test'
            })

            assert.fail('Should fail')
        } catch (err) {
            console.log(err)
            assert.equal(err.code, 'OPERATION_NOT_PERFORMED', 'Bad error code')
        }
    })
})