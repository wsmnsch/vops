# VOPS

Allowed to standardize operations with everything and store it in any store.

Build-in:

* input validation with json-schema
* access control
* operations history
* extra updates

## Describe operation

#### **`updateFoo.js`**
```javascript
function (config) {
    return {
        name: 'updateFoo',
        schema: {
            "$schema": "http://json-schema.org/draft-07/schema#",
            "type": "object",
            "properties": {
                "foo": {
                    "type": ["null", "string"]
                }
            },
            "additionalProperties": false
        },
        resolveMeta: function (op, doc, options) {
            return {
                bar: doc.bar
            }
        },
        resolveDone: function (op, doc, options) {
            return 1
        },
        resolveExtra: function (op, doc, options) {
            return {
                criteria: {
                    bar: 'baz'
                },
                update: {
                    foo: op.input.foo || config.defaultFoo
                }
            }
        },
        afterOp: function (op, doc, options) {
            //make notification
        }
    }
}
```

## Init vops

```javascript
const Vops = require('vops')
const updateFoo = require('./updateFoo')
const ObjectID = require('mongodb').ObjectID

const vops = new Vops({
    opsFieldName: 'vops', //default 'ops'
    config: {
        defaultFoo: 'bar'
    },
    operations: [
        updateFoo
    ],
    find: function () {

    },
    update: function () {

    },
    updateOp: function (doc, op, update) {
        const criteria = { _id: doc._id }
        criteria['ops._id'] = new ObjectID(op._id)

        const update = {'$set': {}}
        _.each(_.keys(attrs), function (key) {
            update['$set']['ops.$.' + key] = attrs[key]
        })

        return update(criteria, update)
    },
    ctx: {},
    isAccessGranted: function () {},
    initOp : function (ctx) {
        return {
            _id: ObjectId(),
            user: ctx.user
        }
    },
    initCriteria : function (doc) {
        return {
            _id: doc._id
        }
    },
    initUpdate : function (op) {
        return {
            $push: {
                'ops': op
            }
        }
    },
})
```

## Make operation with vops

```javascript
const doc = {
    id: 1,
    foo: 'foo',
    bar: 'baz',
}

const opetationResult = await vops.makeOp(doc, {
    name: 'updateFoo',
    input: {
        foo: 'baz'
    }
})
```

## Errors

|Error code|Description|
|---|---|
|OPERATION_NOT_PERFORMED|.update returns nModified not equal 1|
|FORBIDDEN|.isGranted returns false|
|VALIDATION_ERROR|operation input failed schema validation|
