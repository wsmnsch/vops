const _ = require('lodash')

module.exports = function ({
    isAccessGranted
}) {
    return async function (op, options = {}) {
        if (options.force) { return true }

        if (!_.isFunction(isAccessGranted)) { return true}

        const hasAccess = await isAccessGranted(_.get(this, 'ctx.user'), 'op:' + op.name)

        if (hasAccess) { return true }

        const error = new Error(`Operation "${op.name}" is not allowed`)
        error.code = 'FORBIDDEN'

        throw error
    }
}