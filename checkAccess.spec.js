const assert = require('assert')
const CheckAccess = require('./checkAccess')

describe('checkAccess', function () {

    it('returns error.code FORBIDDEN if .isAccessGranted returns false', async function () {

        const checkAccess = CheckAccess({
            isAccessGranted: function () { return false }
        })

        try {
            await checkAccess.call({}, { name: 'test' })

            assert.fail('Should fail')
        } catch (err) {
            assert.equal(err.code, 'FORBIDDEN', 'Bad error code')
        }
    })
})