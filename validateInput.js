const _ = require('lodash')
const Ajv = require('ajv')

module.exports = function ({
    ops
}) {

    const validators = createValidators(ops)

    return async function (op) {
        const isValid = validators[op.name]

        if (!_.isFunction(isValid)) { return true }

        if (isValid(op.input)) { return true }

        const error = new Error(JSON.stringify(isValid.errors))
        error.code = 'INPUT_IS_NOT_VALID'

        throw error
    }
}

const ajv = new Ajv({ allErrors: true, schemaId: 'auto'})

function createValidators (ops) {
    return _.mapValues(ops, function (op) {

        if (!_.isObject(op.schema)) { return null }

        return ajv.compile(op.schema)
    })
}