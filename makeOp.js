const _ = require('lodash')

module.exports = function ({
    find,
    update,
    updateOp,
    initOp,
    initCriteria,
    initUpdate,

    checkAccess,
    validateInput,
    resolveMeta,
    resolveDone,
    resolveExtra,
    afterOp,
    ctx
}) {

    if (
        !_.isFunction(initOp) ||
        !_.isFunction(initCriteria) ||
        !_.isFunction(initUpdate) ||
        !_.isFunction(update) ||
        !_.isFunction(find) ||
        !_.isFunction(updateOp)
    ) {
        const error = new Error(`
            Empty required params for makeOp.
            Required params:
            {
                initOp : function (ctx) {},
                initCriteria : function (doc) {},
                initUpdate : function (op) {},
                update : function (criteria, update) {},
                find : function (doc) {},
                updateOp : function (doc, op, update) {},
            }
        `)
        error.code = 'REQUIRED PARAMS IS EMPTY'
        throw error
    }

    return async function (doc, op, options) {

        op = _.merge(initOp(ctx), {
            date: new Date(),
            outcome: {}
        }, op)

        if (_.isFunction(checkAccess)) {
            await checkAccess.call(ctx, op)
        }

        if (_.isFunction(validateInput)) {
            await validateInput.call(ctx, op)
        }

        if (_.isFunction(resolveMeta)) {
            const meta = await resolveMeta.call(this, op, doc, null, null, options)
            if (meta) { op.meta = meta }
        }

        if (op.withState) {

            op.state = 'inprog'

            const criteria = _.merge(initCriteria.call(ctx, doc))
            const updateProps = _.merge(initUpdate.call(ctx, op))

            const isUpdated = await update(criteria, updateProps)

            if (!isUpdated) {
                const error = new Error(`Operation "${op.name}" start failed for ${ JSON.stringify(criteria) }`)
                error.code = 'OPERATION_START_FAILED'
                throw error
            }

            try {
                op.done = await resolveDone.call(this, op, doc, null, null, options)
            } catch (err) {
                op.done = 0
                op.error = err.name + ': ' + err.message
                op.err = JSON.stringify(err)
            }

            const extra = await resolveExtra.call(ctx, op, doc, null, null, options)

            if (extra) {
                await update(ini)
            }

            op.state = 'processed'
            op.processedAt = new Date()
            op.executiontime = op.processedAt - op.date

            await updateOp(doc, op, op)

            return op

        } else {

            try {
                op.done = await resolveDone.call(this, op, doc, null, null, options)
            } catch (err) {
                op.done = 0
                op.error = err.name + ': ' + err.message
                op.err = JSON.stringify(err)
            }

            const extra = await resolveExtra.call(ctx, op, doc, null, null, options)

            const criteria = _.merge(initCriteria.call(ctx, doc), extra && extra.criteria)
            const updateProps = _.merge(initUpdate.call(ctx, op), extra && extra.update)

            const isUpdated = await update(criteria, updateProps)

            if (!isUpdated) {
                const error = new Error(`Operation "${op.name}" not performed for ${ JSON.stringify(criteria) }`)
                error.code = 'OPERATION_NOT_PERFORMED'
                throw error
            }

            if (_.isFunction(afterOp)) {
                try {
                    doc = await find(doc)
                    await afterOp.call(ctx, op, doc, options)
                } catch (err) {
                    await updateOp(doc, op, {
                        afterError: {
                            message: err.name + ': ' + err.message,
                            err: JSON.stringify(err)
                        }
                    })
                }
            }

            return op
        }

    }
}