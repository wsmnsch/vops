const _ = require('lodash')


module.exports = function ({
    operations, config,
    find, update, updateOp,
    isAccessGranted,
    initOp,
    initCriteria,
    initUpdate,
}) {
    
    const ops = reduceOperations(operations, config)

    return class Vops {

        constructor ({
            ctx
        }) {
    
            this.ops = ops
    
            const checkAccess = require('./checkAccess')({ isAccessGranted })
            const validateInput = require('./validateInput')({ ops })
            const resolveMeta = require('./resolveMeta')({ ctx, ops })
            const resolveDone = require('./resolveDone')({ ctx, ops })
            const resolveExtra = require('./resolveExtra')({ ctx, ops })
            const afterOp = require('./afterOp')({ ctx, ops })
    
            this.makeOp = require('./makeOp')({
                ctx,
                find, update, updateOp,
    
                checkAccess,
                validateInput,
                resolveMeta,
                resolveDone,
                resolveExtra,
                afterOp,
    
                initOp,
                initCriteria,
                initUpdate,
            })
        }
    }
}



function reduceOperations (operations = [], config = {}) {
    return _.reduce(operations, function (ops, opConstructor) {
        const op = opConstructor(config)
        ops[op.name] = op
        return ops
    }, {})
}
