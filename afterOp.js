module.exports = function ({
    ops
}) {

    return async function (op, doc, subdoc, subdocId) {
        return Promise.resolve()
        .then(() => {
            if (typeof op !== 'object') { return }

            const opDefinition = ops[op.name]
            const after = opDefinition && opDefinition.after

            if (typeof after === 'function') {
                return after.call(this, op, doc, subdoc, subdocId)
            }
        })
        .then(() => {
            if (typeof this.afterOp === 'function') {
                return this.afterOp(op, doc, subdoc, subdocId)
            }
        })
    }
}