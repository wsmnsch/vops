module.exports = function ({
    ops, ctx
}) {
    return async function (op, doc, subdoc, subdocId, options) {
        const opDefinition = ops[op.name]
        const opExtraResolver = opDefinition && opDefinition.resolveExtra

        if (typeof opExtraResolver !== 'function') { return opExtraResolver || {} }

        return opExtraResolver.call(this, op, doc, subdoc, subdocId, options)
    }
}