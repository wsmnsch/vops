module.exports = function ({
    ops, ctx
}) {
    return async function (op, doc, subdoc, subdocId, options) {
        const opDefinition = ops[op.name]
        const opDoneResolver = opDefinition && opDefinition.resolveDone

        if (typeof(opDoneResolver) !== 'function') {

            if (typeof(op.done) !== 'undefined') { return op.done }

            return 1
        }

        return opDoneResolver.call(ctx, op, doc, subdoc, subdocId, options)
    }
}