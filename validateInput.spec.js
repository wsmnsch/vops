const assert = require('assert')
const ValidateInput = require('./validateInput')

describe('validateInput', function () {

    it('returns error.code INPUT_IS_NOT_VALID if json-schema validation failed', async function () {

        const validateInput = ValidateInput({
            ops: {
                'test': {
                    schema: {
                        "$schema": "http://json-schema.org/draft-07/schema#",
                        "type": "object",
                        "properties": {
                            "a": {"type": "string"},
                        },
                    }
                }
            }
        })

        try {
            await validateInput.call({}, {
                name: 'test',
                input: {
                    a: 1
                }
            })

            assert.fail('Should fail')
        } catch (err) {
            assert.equal(err.code, 'INPUT_IS_NOT_VALID', 'Bad error code')
        }
    })

    it('returns true if json-schema validated', async function () {

        const validateInput = ValidateInput({
            ops: {
                'test': {
                    schema: {
                        "$schema": "http://json-schema.org/draft-07/schema#",
                        "type": "object",
                        "properties": {
                            "a": {"type": "string"},
                        },
                    }
                }
            }
        })

        const result = await validateInput.call({}, {
            name: 'test',
            input: {
                a: '1'
            }
        })

        assert.equal(result, true)
    })
})