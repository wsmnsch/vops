module.exports = function ({
    ops, ctx
}) {
    return async function (op, doc, subdoc, subdocId, options) {
        const opDefinition = ops[op.name]
        const opMetaResolver = opDefinition && opDefinition.resolveMeta

        if (typeof (opMetaResolver) !== 'function') { return opMetaResolver }

        return opMetaResolver.call(ctx, op, doc, subdoc, subdocId, options)
    }
}